#!/bin/bash -e
#
# GitLab docker instance registry cleanup script. The idea is to call
# it from a cron job to periodically prune unwanted objects from the
# ever growing docker registry.
#
# Use at your own risks, it can potentially delete a lot of things.
#

# Standard GitLab data path
DATADIR="/var/opt/gitlab"

# Exit with an error message
die () {
    echo "Error: $*" >&2
    exit 1
}

# Display an information message
info () {
    echo "Info: $*"
}

# Remove stuff
remove () {
    rm -rfv -- "$@"
}

##
## Clean a single registry repository
##
clean_repo () {
    local wanted_manifests=()
    local wanted_layers=()
    cd "$1"

    ##
    ## Step #1: remove obsolete manifests, by tag
    ##
    for tagdir in _manifests/tags/*; do
	tag="${tagdir##*/}"

	# It could be that there's no tag
	[ "$tag" = "*" ] && break

	# Preserve the hash of the current image
	current="$(cut -d: -f2 < "$tagdir/current/link")"
	wanted_manifests+=("$current")
	info "tag $tag is at $current"

	# Delete all other manifests
	for i in $tagdir/index/sha256/*; do
	    if [ "$(basename "$i")" = "$current" ]; then
		info "preserving $i"
	    else
		remove "$i"
	    fi
	done
    done

    ##
    ## Step #2: remove obsolete manifests, by revision
    ##
    for revdir in _manifests/revisions/sha256/*; do
	rev="${revdir##*/}"

	# It could be that there's no manifest
	[ "$rev" = "*" ] && break

	# Preserve this manifest?
	found=0
	for i in "${wanted_manifests[@]}"; do
	    [ "$rev" = "$i" ] && found=1
	done

	# Delete all other obsolete manifests
	if [ "$found" = 1 ]; then
	    info "preserving $revdir"
	else
	    remove "$revdir"
	fi
    done

    ##
    ## Step #3: remove obsolete layers
    ##
    for manifest in "${wanted_manifests[@]}"; do

	# Locate the blob data file
	blobfile="$blobdir/$(echo "$manifest" | cut -c -2)/$manifest/data"
	[ -f "$blobfile" ] || error "File $blobfile doesn't exist."

	# Remember all referenced layers (greedy and brutal)
	readarray -t layers < <(grep 'sha256:' < "$blobfile"		\
				    | sed -e 's/"digest": "sha256://'	\
					  -e 's/[ \t]*//'		\
					  -e 's/"//g')
	wanted_layers+=( "${layers[@]}" )
    done

    for layerdir in _layers/sha256/*; do
	layer="${layerdir##*/}"

	# It could be that there's no layer
	[ "$layer" = "*" ] && break

	# Preserve this layer?
	found=0
	for i in "${wanted_layers[@]}"; do
	    [ "$layer" = "$i" ] && found=1
	done

	# Delete all other obsolete layers
	if [ $found = 1 ]; then
	    info "preserving $layerdir"
	else
	    remove "$layerdir"
	fi
    done
}

##
## Entry point
##

# Make sure that the data directory exists
[ -d "$DATADIR" ] || die "Can't find the GitLab data directory"

# Make sure that we have write permissions to it
[ -w "$DATADIR" ] || die "Insufficient permissions to the GitLab data directory"

# Figure out the path to the registry
regdir="$DATADIR/gitlab-rails/shared/registry/docker/registry/v2"
blobdir="$regdir/blobs/sha256"
repodir="$regdir/repositories"
[ -d "$blobdir" ] || die "Can't find the registry blob directory"
[ -d "$repodir" ] || die "Can't find the registry repo directory"

# Lookup all registy repositories
readarray -t repos < <(find "$repodir" -type d								\
			    -exec test -d '{}'/_layers -a -d '{}'/_uploads -a -d '{}'/_manifests \;	\
			    \( -prune -print \))

# Iterate over each repository
for repo in "${repos[@]}"; do
    clean_repo "$repo"
done

# Call the registry garbage collector
exec gitlab-ctl registry-garbage-collect
